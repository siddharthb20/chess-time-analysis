"""
PGN processing tools

@author: Pratik
"""

import chess.pgn
import io
import re

def chesscom_pgn_parser(text):
    
    """
    Returns a list of chess.pgn.Game objects parsed from 'text' pgn string
    
    Parameters
    ----------
    text: str
        String containing one/multiple chess games in PGN format (ususally as returned by Chess.com api)
        
    Returns
    -------
    games: list
        A list of games (chess.pgn.Game object) played by the user on Chess.com
        
    headers: list
        A list of header strings associated with games
        
    pgn_text: list
        A list of pgn moves strings associated with games
    """
    
    
    splits = text.split("\n\n")

    headers = []
    pgn_text = []
    
    for i in range(len(splits)):
        
        if i%2 == 0:
            
            # headers.append(splits[i])
            headers = headers + [splits[i]]

        else:
            # pgn_text.append(splits[i])
            pgn_text = pgn_text + [splits[i]]

    if len(headers)==len(pgn_text):
        
        games = list()

        for j in range(0, len(headers)):
                        
            games.append(chess.pgn.read_game(io.StringIO(headers[j] + pgn_text[j])))        
        
        return games, headers, pgn_text
    
        
    else:
    
        return "Parsing Failed"
    
    
def strip_click_time(text):
    
    """
    Returns a pgn string of moves without the click times appended by chess.com
    
    Parameters
    ----------
    text: str
        String containing one chess game in PGN format (ususally as returned by Chess.com api)
        
    Returns
    -------
    text: str
        A string of pgn moves without the click times 
    """
    
    text = re.sub("{.*?}", "", text)    # removes curly brackets and everything within
    text = re.sub(" .{1,2}\.\.\.", "", text) # removes 1... , 2... etc.
    text = re.sub("  ", " ", text) # replaces double blanks with single blanks
    
    return text
    
    
