"""
Chess.com User 


@author: Pratik
"""

import requests
import json

import pandas as pd

from chesscompy.paths import Paths
from chesscompy.pgn_processing import chesscom_pgn_parser

class User():
    
    """
    Creates a chess.com user object
    
    Parameters
    ----------
    username : str
        Chess.com username of the user
    
    
    Attributes
    ----------
    username : str
        Chess.com username of the user
        
    paths: chesscompy.Paths
        contains url string attributes to resources in Chess.com api
        
    
    Methods
    -------
    __init__(username)
        Initializes a User object with username passed as string
        
    get_games_by_month(year="2020", month="08", url="", from_url=False)
        Returns the games archive of given month for the user through api GET request
        
    get_archived_games()
        Returns all archived games for the user
    
    get_headers(games, as_dataframe=True)
        Returns a list of headers for games(chess.pgn.Game) passed as input
        
    enrich_dataframe(games, pgn_text)
        Returns a list of headers for games(chess.pgn.Game) passed as input with enriched data
    """

    
    def __init__(self, username):
        
        """
        Initializes a User object with username passed as string
        

        Parameters
        ----------
        username : str
            Chess.com username of the user
        """
            
        self.username = username
        self.paths = Paths()
        
        
        
   
    def get_games_by_month(self, year="2020", month="08", url="", from_url=False):
        
        """
        Returns the games archive of given month for the user through api GET request
        

        Parameters
        ----------
        year : str
            Year of the monthly archive
        
        month : str
            Month of the monthly arcive
        
        url : str, optional
            URL string to be used to seng GET request
            
        from_url: bool, optional
            If true, 'url' is used to send GET request
            
        Returns
        -------
        response: requests.response
            Response object returned by GET request
        
        """
        
        if from_url:
            
            urlString = url
        else:
            
            urlString = self.paths.player + "{}/games/{}/{}/pgn".format(self.username, year, month)
        
        response = requests.get(urlString).text
        
        return response
    
    
    def get_archived_games(self):
        
        """
        Returns all archived games for the user
        
            
        Returns
        -------
        games: list
            A list of games (chess.pgn.Game object) played by the user on Chess.com
            
        headers: list
            A list of header strings associated with games
        
        pgn_text: list
            A list of pgn moves strings associated with games
        """
        
        urlArchives = self.paths.player + "{}/games/archives".format(self.username)
        
        response = json.loads(requests.get(urlArchives).text)
        
        games = []
        headers = []
        pgn_text = []
        
        for arch in response["archives"]:
            
            games_list, headers_list, pgn_text_list = chesscom_pgn_parser(self.get_games_by_month(url=arch+"/pgn", from_url=True))
            
            games = games + games_list
            headers = headers + headers_list
            pgn_text = pgn_text + pgn_text_list
        
        return games, headers, pgn_text
    
    @classmethod
    def get_headers(cls, games, as_dataframe=True):
        
        """
        Returns a list of headers for games(chess.pgn.Game) passed as input
        
        Parameters
        ----------
        games : list
            list of games (chess.pgn.Game object)
            
        as_dataframe: bool, optional
            If True, returns a dataframe of headers
            
        Returns
        -------
        headers_list: list
            A list of dict() each containing headers for respective games
        """
        headers_list = []
            
        for g in games:
                
            headers_list.append(dict(g.headers))

        if as_dataframe:
            
            headers_df = pd.DataFrame.from_dict(headers_list)
            
            return headers_df
        
        else:
            
            return headers_list
        
        
    def enrich_dataframe(self, games, pgn_text):
        
        
        """
        Returns a list of headers for games(chess.pgn.Game) passed as input with enriched data
        
        Parameters
        ----------
        games : list
            list of games (chess.pgn.Game object)
            
        pgn_text: list
            List of Strings containing pgn games
            
        Returns
        -------
        df: pandas.DataFrame
            A dataframe of games
        """
        
        df = User.get_headers(games, as_dataframe=True)
        
        """ get pgn moves and pgn object chess.pgn.Game"""
        df.loc[:, "pgn_moves_clk"] = pd.Series(pgn_text)
        
        df.loc[:, "pgn_obj"] = pd.Series(games)
        
        color_dict = {"Black": 0,
                      "White": 1}
        
        result_dict = {"1-0": {"White": 1, "Black": -1},
                       "1/2-1/2": {"White": 0, "Black": 0},
                       "0-1": {"White": -1, "Black": 1}}
        
        
        
        for index, row in df.iterrows():
            
            """get user color and result"""
            
            if row["White"] == self.username:
                df.loc[index, "user_color"] = color_dict["White"]
                df.loc[index, "user_result"] = result_dict[row["Result"]]["White"]
        
            else:
                df.loc[index, "user_color"] = color_dict["Black"]
                df.loc[index, "user_result"] = result_dict[row["Result"]]["Black"]
            
            """get opening move"""
            try:
                df.loc[index, "opening_move"] = row["pgn_moves_clk"].split()[1]
            except IndexError:
                df.loc[index, "opening_move"] = "no moves played"
            
            """get time control category"""
            try:
                tc = int(row["TimeControl"].split("+")[0])
                df.loc[index, "base_time"] = tc
                try:
                    inc = int(row["TimeControl"].split("+")[1])
                    df.loc[index, "increment"] = inc
                except IndexError:
                    df.loc[index, "increment"] = 0
                
                if  tc <= 60: 
                    df.loc[index, "time_control"] = "Bullet"
                elif tc <600:
                    df.loc[index, "time_control"] = "Blitz"
                else:
                    df.loc[index, "time_control"] = "Rapid"
                    
            except ValueError:
                df.loc[index, "base_time"] = 0
                df.loc[index, "increment"] = 0
                df.loc[index, "time_control"] = "Daily"
                
            """get cause of termination"""
            df.loc[index, "termination_cause"] = row["Termination"].split()[-1]
                
            
        return df
    
    
    def user_stats_api(self):
        
        """
        Returns the complete chess.com statistics of the user
        
        Parameters
        ----------

            
        Returns
        -------
        stats: dict
            A dictionary of chess.com statistics for different time controls
        """
        
        urlString = self.paths.player + r"{}/stats".format(self.username)

        response = requests.get(urlString).text
        
        stats = json.loads(response)
        
        return stats
    
    

            
        
                
        
        
        
        