# -*- coding: utf-8 -*-
"""
Created on Sun Dec  6 08:57:46 2020

@author: Siddharth
"""

import numpy as np
import matplotlib.pyplot as plt
from .move_timestamps import time_stamps
from .engine_result import engine_eval
from .move_accuracy import move_accuracy
    
                        
def data_organization (df, games, GAME_NUMBER, ENGINE_PATH):
    
    """
    Creates 3D tensor for input to NN
    
    Parameters
    ----------
    df: dataframe
        Dataframe containing all game data for a user in a month
    
    games: list
        List of all game objects for every game
        
    GAME_NUMBER: int
        The serial number of the game to be analyzed
        
    ENGINE_PATH: str
        Location of the stockfish file on the users local drive
        
    Returns
    -------
    
    """
    
    #print("Printing from create_matrix.py")
    #time_controls = list(df.TimeControl.unique())
    TIME_CONTROL = 60
    #GAME_NUMBER = 5
    
    TIME_CONTROL = int(df["TimeControl"][GAME_NUMBER])
    
    # Matrix to store move number vs time left data
    time_move_matrix = np.zeros((TIME_CONTROL*10, TIME_CONTROL*10))
    
    if df["user_color"][GAME_NUMBER]:
        _, timestamps = time_stamps(df, GAME_NUMBER)
    
    else:
        timestamps, _ = time_stamps(df, GAME_NUMBER)
        
    for index, ele in enumerate(timestamps):
        
        for j in range(TIME_CONTROL*10):
            
            if timestamps[index] == (TIME_CONTROL*10 - j - 1)/10:
                time_move_matrix[index, j] = 1
                break
    
    #print(time_move_matrix)
    game = games[GAME_NUMBER]
    player_move_eval, engine_move_eval = engine_eval(df, game, ENGINE_PATH)
    accuracy_matrix = move_accuracy(player_move_eval, engine_move_eval)
    #print("Player move evaluations:", player_move_eval)