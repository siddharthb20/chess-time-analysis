# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 18:40:16 2020

@author: Siddharth
"""

def openings_white(pgn, results, user_color, first_move):
       
       e4_games = []
       color = []
       
       search_string = '1. ' + first_move
       
       e4_wins, e4_draws, e4_losses = [], [], []
       non_e4_wins, non_e4_draws, non_e4_losses = [], [], []
       wins, losses, draws = [], [], []
       
       for ind, game in enumerate(pgn):
           
           if game.find(search_string) != -1 and user_color[ind] == 'W':
               
               e4_games.append(1)
               
           else:
               
               e4_games.append(0)
                   
           if results[ind] == 'W':
               
               wins.append(1)
               
           else:
               
               wins.append(0)
               
           if results[ind] == 'D':
               
               draws.append(1)
               
           else:
               
               draws.append(0)
               
           if results[ind] == 'L':
               
               losses.append(1)
               
           else:
               
               losses.append(0)
               
           if user_color[ind] == 'W':
               
               color.append(1)
               
           else:
               
               color.append(0)
        
       for a, c in zip(wins, e4_games):
           
           e4_wins.append(a * c)
           non_e4_wins.append(a * (1-c))
                    
       for a, c in zip(losses, e4_games):
           
           e4_losses.append(a * c)
           non_e4_losses.append(a * (1-c))
           
       for a, c in zip(draws, e4_games):
           
           e4_draws.append(a * c)
           non_e4_draws.append(a * (1-c))  
           
         
       print("\n\n% of games when you played {}: {}".format(first_move, e4_games.count(1)*100/color.count(1)))
       print ("\n% of games won when you played {}: {}".format(first_move, e4_wins.count(1)*100/e4_games.count(1)))
       #print ("% of games won when you didn't play e4: ", non_e4_wins.count(1)*100/e4_games.count(1))
    
       print ("\n% of games drawn when you played {}: {}".format(first_move, e4_draws.count(1)*100/e4_games.count(1)))
       #print ("% of games drawn when you didn't play e4: ", non_e4_draws.count(1)*100/e4_games.count(1))
       
       print ("\n% of games lost when you played {}: {}".format(first_move, e4_losses.count(1)*100/e4_games.count(1)))
       #print ("% of games lost when you didn't play e4: ", non_e4_losses.count(1)*100/e4_games.count(1))
       
       
       
       
def openings_black(pgn, results, user_color, first_move):
       
       e5_games = []
       color = []
       
       search_string = '1... ' + first_move
       
       e5_wins, e5_draws, e5_losses = [], [], []
       non_e5_wins, non_e5_draws, non_e5_losses = [], [], []
       wins, losses, draws = [], [], []
       
       
       # Convert wins, draws, losses and opening move to lists of 1s and 0s
       for ind, game in enumerate(pgn):
           
           if game.find(search_string) != -1 and user_color[ind] == 'B':
               
               e5_games.append(1)
               
           else:
               
               e5_games.append(0)
                   
           if results[ind] == 'W':
               
               wins.append(1)
               
           else:
               
               wins.append(0)
               
           if results[ind] == 'D':
               
               draws.append(1)
               
           else:
               
               draws.append(0)
               
           if results[ind] == 'L':
               
               losses.append(1)
               
           else:
               
               losses.append(0)
               
           if user_color[ind] == 'B':
               
               color.append(1)
               
           else:
               
               color.append(0)
        
        
       # Multiply list elements to generate case specific statistic 
       for a, c in zip(wins, e5_games):
           
           e5_wins.append(a * c)
           non_e5_wins.append(a * (1-c))
                    
       for a, c in zip(losses, e5_games):
           
           e5_losses.append(a * c)
           non_e5_losses.append(a * (1-c))
           
       for a, c in zip(draws, e5_games):
           
           e5_draws.append(a * c)
           non_e5_draws.append(a * (1-c))  
           
         
       print("\n\n% of games when you played {}: {}".format(first_move, e5_games.count(1)*100/color.count(1)))
       print ("\n% of games won when you played {}: {}".format(first_move, e5_wins.count(1)*100/e5_games.count(1)))
       #print ("% of games won when you didn't play e4: ", non_e4_wins.count(1)*100/e4_games.count(1))
    
       print ("\n% of games drawn when you played {}: {}".format(first_move, e5_draws.count(1)*100/e5_games.count(1)))
       #print ("% of games drawn when you didn't play e4: ", non_e4_draws.count(1)*100/e4_games.count(1))
       
       print ("\n% of games lost when you played {}: {}".format(first_move, e5_losses.count(1)*100/e5_games.count(1)))
       #print ("% of games lost when you didn't play e4: ", non_e4_losses.count(1)*100/e4_games.count(1))
       