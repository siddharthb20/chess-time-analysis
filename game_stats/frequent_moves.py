# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 09:19:48 2020

@author: Siddharth
"""

import chess
import chess.pgn
import re
import collections

#print("\n\nPrinting from frequent_moves.py")

def freq_moves(df):
    
    """
    Returns two items specifying the most common opening moves as black and white
    
    Parameters
    ----------
    df : dataframe with game data of the user
        Data from chess.com arranged into the dataframe for further usage
        
        
    Returns
    -------
       
    
    """
    
    white, black = [], []
    
    for i in range(df["Event"].count()):
        text = df["pgn_moves_clk"][i]
        text = re.sub("{.*?}", "", text)    # removes curly brackets and everything within
        text = re.sub(" .{1,2}\.\.\.", "", text) # removes 1... , 2... etc.
        text = re.sub("  ", " ", text) # replaces double blanks with single blanks
        first_move = re.search('1. (.+?)2.', text).group(1)
        first_move_white, first_move_black = first_move.split()[0], first_move.split()[1]
        
        if df["user_color"][i]:
            white.append(first_move_white)
        
        else:
            black.append(first_move_black)
            
    #print("\n\nYour most common opening moves as white: ", collections.Counter(white).most_common(1) )
    #print("\nYour most common opening moves as black: ", collections.Counter(black).most_common(1) )