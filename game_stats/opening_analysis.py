# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 04:16:01 2020

@author: Siddharth
"""

from .first_move import openings_white, openings_black
from .frequent_moves import freq_moves
from .opening_name import opening_name
import chess
import chess.pgn

def opening(df, games):
    
    
    #freq_moves(df)  
    opening_type = opening_name(df, games)   
    
    opening_type.sort(key = lambda x: len(x[1]), reverse= True)
    
    for i in range(len(opening_type)):
        
        if len(opening_type[i][1]) == 0:
            continue
        
        print("\n", opening_type[i][0], len(opening_type[i][1]), "\b:", opening_type[i][1])