# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 08:25:54 2020

@author: Siddharth
"""

def opening_name(df, games):
    
    """
    Gives some popular openings and the number(index) of games in which those were played 
    by the user
    
    Parameters
    ----------
    df: dataframe
        Dataframe containing all game data for a user in a month
    
    games: list 
        List of all game files for extracting FENs
            
    Returns
    -------
    opening: list
        List of popular openings and the games in which the user played them for further analysis
        
    """
    
    opening = []
    opening.append(["King's Indian Defense", kings_indian(df, games)])
    opening.append(["Petrov/Russian Defense", petrov(df, games)])
    opening.append(["Sicilian Defense", sicilian(df, games)])
    opening.append(["Italian", italian(df, games)])
    opening.append(["Ruy Lopez/Spanish", ruy_lopez(df, games)])
    opening.append(["Queens Gambit", queens_gambit(df, games)])
    opening.append(["Kings Gambit", kings_gambit(df, games)])
    opening.append(["Fried Liver attack", fried_liver(df, games)])
    opening.append(["Traxler Counter attack", traxler(df, games)])
    opening.append(["Berlin defense", berlin(df, games)])
    opening.append(["English opening", english(df, games)])
    opening.append(["Center game", center(df, games)])
    
    return opening
    
    
def kings_indian(df, games):
    
    kings_indian = []
    kings_indian_after_5 = ['rnbq1rk1', 'ppp1ppbp', '3p1np1']
    kings_indian_after_4 = ['rnbq1rk1', 'ppppppbp', '5np1']
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 1:
            continue
        
        
        # Checking for King's Indian right away
        for i in range(8):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(kings_indian_after_4[i]) != -1:
            i += 1
            if i == len(kings_indian_after_4):
                break
        
        if i == len(kings_indian_after_4):
            kings_indian.append(GAME_NUMBER)
            
        # Checking for the Pirc defense cases which transpose into King's Indian
        for j in range(2):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        while node.board().fen().find(kings_indian_after_5[i]) != -1:
            i += 1
            if i == len(kings_indian_after_5):
                break
        
        if i == len(kings_indian_after_5):
            kings_indian.append(GAME_NUMBER)
            
    return kings_indian



def petrov(df, games):
    
    petrov = []
    petrov_2 = ['rnbqkb1r', 'pppp1ppp', '5n2', '4p3', '4P3', '5N2', 'PPPP1PPP', 'RNBQKB1R']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 1:
            continue
        
        
        # Checking for Petrov
        for i in range(4):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(petrov_2[i]) != -1:
            i += 1
            if i == len(petrov_2):
                break
        
        if i == len(petrov_2):
            petrov.append(GAME_NUMBER)
            
    return petrov

def italian(df, games):
    
    italian = []
    italian_3 = ['r1bqkbnr', 'pppp1ppp', '2n5', '4p3', '2B1P3', '5N2', 'PPPP1PPP', 'RNBQK2R']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is black
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for Italian game
        for i in range(5):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(italian_3[i]) != -1:
            i += 1
            if i == len(italian_3):
                break
        
        if i == len(italian_3):
            italian.append(GAME_NUMBER)
            
    return italian


def ruy_lopez(df, games):
    
    spanish = []
    spanish_3 = ['r1bqkbnr', 'pppp1ppp', '2n5', '1B2p3', '4P3', '5N2', 'PPPP1PPP', 'RNBQK2R']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for Spanish game
        for i in range(5):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(spanish_3[i]) != -1:
            i += 1
            if i == len(spanish_3):
                break
        
        if i == len(spanish_3):
            spanish.append(GAME_NUMBER)
            
    return spanish


def sicilian(df, games):
    
    sicilian = []
    sicilian_1 = ['2p5']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 1:
            continue
        
        
        # Checking for Sicilian
        for i in range(2):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(sicilian_1[i]) != -1:
            i += 1
            if i == len(sicilian_1):
                break
        
        if i == len(sicilian_1):
            sicilian.append(GAME_NUMBER)
            
    return sicilian


def queens_gambit(df, games):
    
    queen = []
    queen_2 = ['2PP4', '8', 'PP2PPPP', 'RNBQKBNR']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is black
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for Queen's Gambit
        for i in range(3):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(queen_2[i]) != -1:
            i += 1
            if i == len(queen_2):
                break
        
        if i == len(queen_2):
            queen.append(GAME_NUMBER)
            
    return queen


def kings_gambit(df, games):
    
    king = []
    king_2 = ['4PP2', '8', 'PPPP2PP', 'RNBQKBNR']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is black
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for King's Gambit
        for i in range(3):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(king_2[i]) != -1:
            i += 1
            if i == len(king_2):
                break
        
        if i == len(king_2):
            king.append(GAME_NUMBER)
            
    return king


def fried_liver(df, games):
    
    f_liver = []
    f_liver_4 = ['r1bqkb1r', 'pppp1ppp', '2n2n2', '4p1N1', '2B1P3', '8', 'PPPP1PPP', 'RNBQK2R']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is black
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for King's Gambit
        for i in range(7):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(f_liver_4[i]) != -1:
            i += 1
            if i == len(f_liver_4):
                break
        
        if i == len(f_liver_4):
            f_liver.append(GAME_NUMBER)
            
    return f_liver


def traxler(df, games):
    
    traxler = []
    traxler_4 = ['r1bqk2r', 'pppp1ppp', '2n2n2', '2b1p1N1', '2B1P3', '8', 'PPPP1PPP', 'RNBQK2R']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 1:
            continue
        
        
        # Checking for King's Gambit
        for i in range(8):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(traxler_4[i]) != -1:
            i += 1
            if i == len(traxler_4):
                break
        
        if i == len(traxler_4):
            traxler.append(GAME_NUMBER)
            
    return traxler



def berlin(df, games):
    
    berlin = []
    berlin_3 = ['r1bqkb1r', 'pppp1ppp', '2n2n2', '1B2p3', '4P3', '5N2', 'PPPP1PPP', 'RNBQK2R']
    berlin_2 = ['rnbqkb1r', 'pppp1ppp', '5n2', '4p3', '2B1P3', '8', 'PPPP1PPP', 'RNBQK1NR']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is white
        if df["user_color"][GAME_NUMBER] == 1:
            continue
        
        
        # Checking for Berlin defense, bishop's opening variation
        for i in range(4):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(berlin_2[i]) != -1:
            i += 1
            if i == len(berlin_2):
                break
        
        if i == len(berlin_2):
            berlin.append(GAME_NUMBER)
            
        # Check for the Ruy Lopez variation of the Berlin defense
        for i in range(2):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(berlin_3[i]) != -1:
            i += 1
            if i == len(berlin_3):
                break
        
        if i == len(berlin_3):
            berlin.append(GAME_NUMBER)
            
    return berlin


def english(df, games):
    
    english = []
    english_1 = ['2P5']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game        
        
        # Checking for English
        for i in range(1):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(english_1[i]) != -1:
            i += 1
            if i == len(english_1):
                break
        
        if i == len(english_1):
            english.append(GAME_NUMBER)
            
    return english


def center(df, games):
    
    center = []
    center_2 = ['rnbqkbnr', 'pppp1ppp', '8', '4p3', '3PP3', '8', 'PPP2PPP', 'RNBQKBNR']
    
    for GAME_NUMBER in range(len(games)):
        
        game = games[GAME_NUMBER]
        node = game
        
        
        # Skipping the cases when user is black
        if df["user_color"][GAME_NUMBER] == 0:
            continue
        
        
        # Checking for Center
        for i in range(3):
            
            if node.is_end() == True:
                break
            node = node.next()
        
        i = 0
        
        while node.board().fen().find(center_2[i]) != -1:
            i += 1
            if i == len(center_2):
                break
        
        if i == len(center_2):
            center.append(GAME_NUMBER)
            
    return center