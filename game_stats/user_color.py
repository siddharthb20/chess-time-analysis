# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 04:30:39 2020

@author: Siddharth
"""

def color(games_headers, username):
    
    search_keyword = "White \"" + username + "\""
    user_color = []
    
    for game in games_headers:
        
        if game.find(search_keyword) != -1:
            
            user_color.append("W")
        
        else:
            
            user_color.append("B")
            
    return user_color