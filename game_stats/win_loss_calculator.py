# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 02:41:00 2020

@author: Siddharth
"""

import pandas as pd

#print("\n\nPrinting from win_loss_calculator.py")

def win_loss (df):
    
    """wins, losses, draws = 0, 0, 0
    results = []
    
    for index, game in enumerate(usercolor):
        
        # If user was white
        if game == 'W':
        
            if games_headers[index].find('Result "1-0"') != -1:
                
                wins += 1
                results.append("W")
            
            elif games_headers[index].find('Result "1/2-1/2"') != -1:
                
                draws += 1
                results.append("D")
                
            else:
                
                losses += 1
                results.append("L")
        
        else:
            
            if games_headers[index].find('Result "0-1"') != -1:
                
                wins += 1
                results.append("W")
            
            elif games_headers[index].find('Result "1/2-1/2"') != -1:
                
                draws += 1
                results.append("D")
                
            else:
                
                losses += 1
                results.append("L")
    """

    print(df["user_result"].value_counts())
    
    #win_percentage = round(wins / len(games_headers) * 100, 2)
    #loss_percentage = round(losses / len(games_headers) * 100, 2)
    #draws_percentage = round(draws / len(games_headers) * 100, 2)
    
    #print("Win %: {} \nLoss %: {} \nDraw %: {}".format(win_percentage, loss_percentage, draws_percentage))
    
    #return results