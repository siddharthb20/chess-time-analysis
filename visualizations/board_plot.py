# -*- coding: utf-8 -*-
"""
Created on Wed Nov 11 10:38:23 2020

@author: Siddharth
"""

import sys
import chess.svg
import chess
from IPython.display import SVG, display


# Storing the board position as an SVG file
def plot_board(board_position):

    print("Board type:", type(board_position))
    outputfile = open('chess_board.svg', "w")
    outputfile.write(board_position)
    outputfile.close()
    
    
# Displaying the SVG file
def show_board(board_position):
    
    plot_board(board_position)
    display(SVG('chess_board.svg'))